import time
import json
import sys, os
# sys.path.append(os.path.abspath("./test_files"))

from test_funcs.pure_with_types import mandelbrot_with_types
from test_funcs.pure_no_types import mandelbrot_no_types
# from test_funcs.np_extended import mandelbrot_np

def test(function):
    tic = time.perf_counter_ns()
    image = function()
    toc = time.perf_counter_ns()
    return (toc - tic) / 1e9

def try_func(function):
    try:
        time = test(function)
    except Exception as e:
        raise e
    return time

def main(arg):
    interpreter = arg[1]

    time_with_types = "err"
    time_no_types = "err"
    time_np = "err"

    time_with_types = try_func(mandelbrot_with_types)
    time_no_types = try_func(mandelbrot_no_types)
    # time_np = try_func(mandelbrot_np)

    response = dict(
        interpreter=interpreter,
        time_with_types=time_with_types,
        time_no_types=time_no_types,
        # time_np=time_np
    )
    result_string = json.dumps(response, indent=4, ensure_ascii=False)
    # f = open("results/{}.txt".format(interpreter), "w", encoding="utf8")
    # print(result_string, file=f)
    # f.close()

    print(result_string)


if __name__ == "__main__":
    main(sys.argv)
