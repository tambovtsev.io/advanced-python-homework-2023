#!/bin/bash

interpreterCmd=$1;
interpreterTitle=$2;
TIMEFORMAT=%R
{
    time $interpreterCmd test_full_with_types.py &&
    time $interpreterCmd test_full_no_types.py &&
    time $interpreterCmd test_full_np.py;
} 2> results/${interpreterTitle}_full.txt;

$interpreterCmd test_func_only.py $interpreterTitle;
