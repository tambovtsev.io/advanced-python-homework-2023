.. Advanced python homework documentation master file, created by
   sphinx-quickstart on Thu Sep 28 21:33:05 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Advanced python homework's documentation!
====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
