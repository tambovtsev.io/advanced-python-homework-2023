# advanced-python-homework-2023
## Student:
[Ilia Tambovtsev](https://t.me/hyperpopa)

## Materials
- [Homework](https://sciprogcentre.github.io/green-courses/advanced_python.html)

## Controls (1st homework)

### Venv creation:
```
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

### Documentation
#### Build
```
cd docs
make.bat
```

#### From scratch
1. run sphinx initialization: `sphinx-quickstart docs`
2. add project packages: `sphinx-apidoc -o docs/source .`
3. edit `conf.py`

```py
import sys
import os
sys.path.insert(0, os.path.abspath("../../"))

extensions = [
    'sphinx.ext.duration',
    'sphinx.ext.doctest',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
]
```
4. add `modules` after `Contents:` in `index.rst`
5. run `make.bat`

## Interpreters (2nd homework)
- [assignment](https://sciprogcentre.github.io/green-courses/pages/advanced_python/2023/hw/02.html)
- [solution](./interpreters)

## Turtle Device (3rd homework)
- [assignment](https://sciprogcentre.github.io/green-courses/pages/advanced_python/2023/hw/03.html)
- [solution](./turtle)

## Turtle Thread (4th homework)
- [assignment](https://sciprogcentre.github.io/green-courses/pages/advanced_python/2023/hw/04.html)
- [solution](./turtle)
