# NonBlocking Turtle Shell
## Problem statement
Create a [non blocking shell](https://sciprogcentre.github.io/green-courses/pages/advanced_python/2023/hw/04.html) for Python Turtle.
## Test
I added two shortcut methods for testing:
- `le` = `execute left 1000` - spinning by 1000$^\circ$
- `fw` = `execute forward 100` - go forward by 100px

`le` takes a lot of time. The shell is not locked while turtle is spinning and you
can enter other commands (e.g. `fw`, `le`). The turtle will do them all
step-by-step.